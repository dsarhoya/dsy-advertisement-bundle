<?php

namespace dsarhoya\DSYAdvertisementBundle\EventSubscriber;

use dsarhoya\DSYAdvertisementBundle\Entity\AdvertisementInterface;
use dsarhoya\DSYFilesBundle\Services\DSYFilesService;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use JMS\Serializer\JsonSerializationVisitor;

/**
 * Serialization Subscriber.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class SerializationSubscriber implements EventSubscriberInterface
{
    protected $filesSrv;

    /**
     * Constructot.
     */
    public function __construct(DSYFilesService $filesSrv)
    {
        $this->filesSrv = $filesSrv;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            ['event' => 'serializer.post_serialize', 'method' => 'onPostSerialize'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function onPostSerialize(ObjectEvent $event)
    {
        $entity = $event->getObject();

        /** @var JsonSerializationVisitor $visitor */
        $visitor = $event->getVisitor();

        if ($entity instanceof AdvertisementInterface) {
            $visitor->setData('image_path_url', $this->filesSrv->fileUrl($entity->getFilePath().'/'.$entity->getImagePath()));
        }
    }
}
