<?php

namespace dsarhoya\DSYAdvertisementBundle\Controller;

use dsarhoya\DSYAdvertisementBundle\Repository\AdvertisementRepository;
use Symfony\Component\HttpFoundation\Response;

/**
 * API Advertisement Controller.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class ApiAdvertisementController extends ApiAbstractController
{
    /**
     * GET Current.
     */
    public function currentAction(): Response
    {
        /** @var AdvertisementRepository $repo */
        $repo = $this->getDoctrine()->getManager()->getRepository($this->getParameter('dsy_advertisement.advertisement_class'));

        $advertisement = $repo->current();

        return $this->serializedResponse($advertisement, ['advertisement_detail']);
    }
}
