<?php

namespace dsarhoya\DSYAdvertisementBundle\Controller;

use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

abstract class ApiAbstractController extends Controller
{
    protected $serializer;

    protected $requestStack;

    /**
     * Constructor.
     */
    public function __construct(SerializerInterface $serializer, RequestStack $requestStack)
    {
        $this->serializer = $serializer;
        $this->requestStack = $requestStack;
    }

    /**
     * serializedResponse function.
     *
     * @param mixed $data
     * @param array $groups
     * @param int   $statusCode
     */
    public function serializedResponse($data, $groups = [], $statusCode = 200): Response
    {
        $response = new Response($this->getJsonSerialize($data, $groups), $statusCode);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @param mixed $data
     * @param array $groups
     */
    public function getJsonSerialize($data, $groups = []): string
    {
        $request = $this->requestStack->getCurrentRequest();

        if (is_array($request->get('expand'))) {
            $groups = array_merge($groups, $request->get('expand'));
        } elseif (is_array($request->get('expand[]'))) {
            $groups = array_merge($groups, $request->get('expand[]'));
        }

        if (is_array($groups) && count($groups)) {
            return $this->serializer->serialize($data, 'json', SerializationContext::create()->setGroups($groups));
        } else {
            return $this->serializer->serialize($data, 'json', SerializationContext::create());
        }
    }
}
