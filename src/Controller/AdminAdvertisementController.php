<?php

namespace dsarhoya\DSYAdvertisementBundle\Controller;

use dsarhoya\DSYAdvertisementBundle\Repository\AdvertisementRepository;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController;

/**
 * Dispatcher Controller.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class AdminAdvertisementController extends AdminController
{
    public function persistEntity($entity)
    {
        $this->disabledAllAdvertisements();
        parent::persistEntity($entity);
    }

    private function disabledAllAdvertisements()
    {
        /** @var AdvertisementRepository $repo */
        $repo = $this->getDoctrine()->getManager()->getRepository($this->getParameter('dsy_advertisement.advertisement_class'));

        $qb = $repo->createQueryBuilder('a');
        $qb->update()->set('a.enabled', $qb->expr()->literal(false));

        $qb->getQuery()->execute();
    }
}
