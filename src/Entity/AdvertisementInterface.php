<?php

namespace dsarhoya\DSYAdvertisementBundle\Entity;

interface AdvertisementInterface
{
    /**
     * Get id.
     *
     * @return int
     */
    public function getId();

    /**
     * Get the value of description.
     *
     * @return string
     */
    public function getDescription();

    /**
     * Set the value of description.
     *
     * @return self
     */
    public function setDescription($description);

    /**
     * Set imagePath.
     *
     * @param string|null $imagePath
     *
     * @return self
     */
    public function setImagePath($imagePath = null);

    /**
     * Get imagePath.
     *
     * @return string|null
     */
    public function getImagePath();

    /**
     * Set intraDayCount.
     *
     * @param int|null $intraDayCount
     *
     * @return self
     */
    public function setIntraDayCount($intraDayCount = null);

    /**
     * Get intraDayCount.
     *
     * @return int|null
     */
    public function getIntraDayCount();

    /**
     * Set dayCount.
     *
     * @param int $dayCount
     *
     * @return self
     */
    public function setDayCount($dayCount = null);

    /**
     * Get dayCount.
     *
     * @return int
     */
    public function getDayCount();

    /**
     * Set fromDate.
     *
     * @param \DateTime|null $fromDate
     *
     * @return self
     */
    public function setFromDate($fromDate = null);

    /**
     * Get fromDate.
     *
     * @return \DateTime|null
     */
    public function getFromDate();

    /**
     * Set toDate.
     *
     * @param \DateTime|null $toDate
     *
     * @return self
     */
    public function setToDate($toDate = null);

    /**
     * Get toDate.
     *
     * @return \DateTime|null
     */
    public function getToDate();

    /**
     * Get the value of enabled.
     *
     * @return bool
     */
    public function getEnabled();

    /**
     * Set the value of enabled.
     *
     * @return self
     */
    public function setEnabled(bool $enabled);

    /**
     * Sets createdAt.
     *
     * @return self
     */
    public function setCreatedAt(\DateTime $createdAt);

    /**
     * Returns createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * Sets updatedAt.
     *
     * @return self
     */
    public function setUpdatedAt(\DateTime $updatedAt);

    /**
     * Returns updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt();

    /**
     * Get file path.
     *
     * @return string|null
     */
    public function getFilePath();

    /**
     * Get file path.
     *
     * @return string|null
     */
    public function getUrl();
}
