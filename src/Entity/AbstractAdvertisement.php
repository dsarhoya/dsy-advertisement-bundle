<?php

namespace dsarhoya\DSYAdvertisementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use dsarhoya\DSYFilesBundle\Interfaces\IFileEnabledEntity;
use dsarhoya\DSYFilesBundle\Services\DSYFilesService;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Advertisement.
 */
abstract class AbstractAdvertisement implements AdvertisementInterface, IFileEnabledEntity
{
    /*
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    const PATH = 'advertisements';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\SerializedName("id")
     * @JMS\Groups({"advertisement_list","advertisement_detail"})
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @JMS\SerializedName("description")
     * @JMS\Groups({"advertisement_list","advertisement_detail"})
     * @Assert\NotBlank
     */
    protected $description;

    /**
     * @ORM\Column(type="boolean", options={"default": 1})
     * @JMS\SerializedName("enabled")
     * @JMS\Groups({"advertisement_detail"})
     *
     * @var bool
     */
    protected $enabled = true;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(message="Debe seleccionar una imagen.")
     */
    protected $imagePath;

    /**
     * @var UploadedFile|null
     * @Assert\File(
     *     maxSize = "2M",
     * )
     */
    protected $file;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer", nullable=true)
     * @JMS\SerializedName("intra_day_count")
     * @JMS\Groups({"advertisement_detail"})
     * @Assert\NotBlank
     */
    protected $intraDayCount = 1;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer", nullable=true)
     * @JMS\SerializedName("day_count")
     * @JMS\Groups({"advertisement_detail"})
     * @Assert\NotBlank
     */
    protected $dayCount = 1;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     * @JMS\SerializedName("from_date")
     * @JMS\Groups({"advertisement_detail"})
     * @Assert\NotBlank
     */
    protected $fromDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     * @JMS\SerializedName("to_date")
     * @JMS\Groups({"advertisement_detail"})
     * @Assert\NotBlank
     */
    protected $toDate;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     * @JMS\SerializedName("url")
     * @JMS\Groups({"advertisement_list","advertisement_detail"})
     */
    protected $url;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description.
     *
     * @return Advertisement
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Set imagePath.
     *
     * @param string|null $imagePath
     *
     * @return Advertisement
     */
    public function setImagePath($imagePath = null)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * Get imagePath.
     *
     * @return string|null
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * Set intraDayCount.
     *
     * @param int|null $intraDayCount
     *
     * @return Advertisement
     */
    public function setIntraDayCount($intraDayCount = null)
    {
        $this->intraDayCount = $intraDayCount;

        return $this;
    }

    /**
     * Get intraDayCount.
     *
     * @return int|null
     */
    public function getIntraDayCount()
    {
        return $this->intraDayCount;
    }

    /**
     * Set dayCount.
     *
     * @param int $dayCount
     *
     * @return Advertisement
     */
    public function setDayCount($dayCount = null)
    {
        $this->dayCount = $dayCount;

        return $this;
    }

    /**
     * Get dayCount.
     *
     * @return int
     */
    public function getDayCount()
    {
        return $this->dayCount;
    }

    /**
     * Set fromDate.
     *
     * @param \DateTime|null $fromDate
     *
     * @return Advertisement
     */
    public function setFromDate($fromDate = null)
    {
        $this->fromDate = $fromDate;

        return $this;
    }

    /**
     * Get fromDate.
     *
     * @return \DateTime|null
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * Set toDate.
     *
     * @param \DateTime|null $toDate
     *
     * @return Advertisement
     */
    public function setToDate($toDate = null)
    {
        $this->toDate = $toDate;

        return $this;
    }

    public function setFile(UploadedFile $file)
    {
        $this->file = $file;
        $ext = (null !== $file->guessExtension()) ? $file->guessExtension() : $file->getClientOriginalExtension();
        $this->setImagePath(sprintf('img_%s.%s', md5(time()), $ext));

        return $this;
    }

    /**
     * Get toDate.
     *
     * @return \DateTime|null
     */
    public function getToDate()
    {
        return $this->toDate;
    }

    /**
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    public function getFileKey()
    {
        return $this->getImagePath();
    }

    public function getFilePath()
    {
        return self::PATH;
    }

    public function getFileProperties()
    {
        return ['ACL' => DSYFilesService::ACL_PUBLIC_READ];
    }

    /**
     * Get the value of enabled.
     *
     * @return bool
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set the value of enabled.
     *
     * @return self
     */
    public function setEnabled(bool $enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get the value of url.
     *
     * @return string|null
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set the value of url.
     *
     * @param string|null $url
     *
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }
}
